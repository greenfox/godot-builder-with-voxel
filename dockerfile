FROM ubuntu:latest

WORKDIR /godotBuild

RUN apt-get update && apt-get install -y \
  build-essential \
  scons \
  pkg-config \
  libx11-dev \
  libxcursor-dev \
  libxinerama-dev \
  libgl1-mesa-dev \
  libglu-dev \
  libasound2-dev \
  libpulse-dev \
  libfreetype6-dev \
  libudev-dev \
  libxi-dev \
  libxrandr-dev \
  yasm \
  git \
  mingw-w64

CMD ["/bin/bash"]
