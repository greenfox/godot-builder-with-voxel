#!/bin/bash

if [ ! -d "godot" ]
then
    git clone https://github.com/godotengine/godot.git
fi
cd godot
git pull

git checkout 3.1.2-stable
