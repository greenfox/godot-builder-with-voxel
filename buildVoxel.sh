#!/bin/bash

run()
{
    updateSource "godot_voxel" "https://github.com/Zylann/godot_voxel.git" "godot3.1"
    updateSource "godot" "https://github.com/godotengine/godot.git" "3.1.2-stable"

    linkModules "godot_voxel" "voxel"

    ./buildGodot.sh $@
}

updateSource()
{
    if [ ! -d "$1" ]
    then
        git clone "$2"
    fi
    pushd $1
    git pull

    git checkout "$3"

    popd

}

linkModules()
{
    ln -s "../../$1" "godot/modules/$2"
}

run $@