#!/bin/bash

pushd godot

THREADS=$(nproc)

scons -j${THREADS} use_static_link=yes platform=x11 tools=yes target=release_debug bits=64 
# scons -j${THREADS} platform=x11 tools=yes target=release_debug bits=64
scons -j${THREADS} use_static_link=yes platform=x11 tools=no target=release bits=64
scons -j${THREADS} use_static_link=yes platform=x11 tools=no target=release_debug bits=64

scons -j${THREADS} use_static_link=yes platform=server tools=yes target=release_debug bits=64
scons -j${THREADS} use_static_link=yes platform=server tools=no target=release bits=64

scons -j${THREADS} use_static_link=yes platform=windows tools=yes target=release_debug bits=64
# scons -j${THREADS} platform=windows tools=yes target=release_debug bits=64
scons -j${THREADS} use_static_link=yes platform=windows tools=no target=release bits=64
scons -j${THREADS} use_static_link=yes platform=windows tools=no target=release_debug bits=64

rm -rf ../out
mv bin ../out

popd
